resource "aws_s3_bucket" "b" {
  bucket = "my-tf-test-bucket-for-circle-ci"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}