##### converts code to zip file

data "archive_file" "my_lambda_function" {
  source_file  = "./index.py"
  output_path = "lambda.zip"
  type        = "zip"
}

#### Lambda Function to Copy from src s3 to dst s3
resource "aws_lambda_function" "hello-world" {
   filename = "${var.env_name}_hello-world"
   source_code_hash = data.archive_file.my_lambda_function.output_base64sha256
   function_name = "${var.env_name}_hello-world"
   role = "${aws_iam_role.hello-world-fnct.arn}"
   handler = "index.lambda_handler"
   runtime = "python3.7"
}


##### POLICIES AND ROLES

resource "aws_iam_role" "hello-world-fnct" {
    name = "app_${var.env_name}_lambda1"

    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_policy" {
  name        = "${var.env_name}_lambda_policy1"
  description = "${var.env_name}_lambda_policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "schemas:*",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams",
        "events:*"

      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "terraform_lambda_iam_policy_basic_execution" {
 role = "${aws_iam_role.hello-world-fnct.id}"
 policy_arn = "${aws_iam_policy.lambda_policy.arn}"
}


resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello-world.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.console.arn
}
