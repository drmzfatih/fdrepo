##### ZIP CODE 

data "archive_file" "my_lambda_function" {
  source_file  = "./lambda_function.py"
  output_path = "lambda.zip"
  type        = "zip"
}


##### LAMBDA FUNCTION

resource "aws_lambda_function" "s3-helloworld" {
   filename = "lambda.zip"
   source_code_hash = data.archive_file.my_lambda_function.output_base64sha256
   role          = aws_iam_role.iam_for_lambda.arn
   function_name = "s3-helloworld"
   handler = "lambda_function.lambda_handler"
   runtime = "python3.8"

}