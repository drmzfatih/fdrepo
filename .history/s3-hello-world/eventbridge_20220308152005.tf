resource "aws_cloudwatch_event_rule" "console" {
  name        = "s3-helloworld"
  description = "s3-helloworld"

  event_pattern = <<EOF
{
  "source": ["aws.s3"],
  "detail-type": ["AWS API Call via CloudTrail"],
  "detail": {
    "eventSource": ["s3.amazonaws.com"],
    "eventName": ["PutObject"],
    "requestParameters": {
      "bucketName": ["tf-test-trail-fatih"]
    }
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "cloudwatch-target" {
  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "CloudWatchoGroup"
  arn       = aws_cloudwatch_log_group.yada.arn
}