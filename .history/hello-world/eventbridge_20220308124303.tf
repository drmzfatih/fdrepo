resource "aws_cloudwatch_event_rule" "console" {
  name        = "Hello-world-rule"
  description = "Capture Hello World every 1 minute"
schedule_expression = "rate(1 minute)"
}

resource "aws_cloudwatch_event_target" "lambda" {
  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "Lambda-hello-world2"
  arn       = aws_lambda_function.hello-world.arn
}