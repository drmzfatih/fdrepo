resource "aws_vpc" "test" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "GoGreen VPC"
  }
}

resource "aws_subnet" "public1" {
  vpc_id                  = aws_vpc.test.id
  cidr_block              = var.subnet_cidrs.public1
  availability_zone       = "us-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "Public1"
  }
}