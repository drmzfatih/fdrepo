resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "archive_file" "python_lambda_package" {  
  type = "zip"  
  source_file = "./lambda_function_s3.py" 
  output_path = "nametest.zip"
}

resource "aws_lambda_function" "test_lambda" {
  filename      = "lambda_function_s3.py"
  function_name = "lambda_function_name"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "index.test"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = filebase64sha256("lambda_function_s3.py")

  runtime = "python3.8"

  environment {
    variables = {
      TARGET_BUCKET = "my-tf-target-bucket-fatih"
    }
  }
}


##### POLICIES AND ROLES

resource "aws_iam_role" "Role-my-buckets" {
  name = "Role-My-Buckets"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda-s3-policy" {
  name        = "lambda-s3-policy"
  description = "My test policy"

  policy = <<EOT
{
        "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::my-tf-source-bucket-fatih/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::my-tf-target-bucket-fatih/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:*"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOT
}

resource "aws_iam_role_policy_attachment" "s3-role-lambda" {
  role       = aws_iam_role.Role-my-buckets.name
  policy_arn = aws_iam_policy.lambda-s3-policy.arn
}

