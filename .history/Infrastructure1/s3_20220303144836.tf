resource "aws_s3_bucket" "a" {
  bucket = "my-tf-source-bucket-fatih"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket" "b" {
  bucket = "my-tf-target-bucket-fatih"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}