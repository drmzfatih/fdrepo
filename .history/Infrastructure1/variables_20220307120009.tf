variable "instance_type" {

}

variable "aws_region" {
    default = "us-east-1"
    description = "AWS Region to deploy to"
}

variable "env_name" {

    description = "Terraform environment name"
}