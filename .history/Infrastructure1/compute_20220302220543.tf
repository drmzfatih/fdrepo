## Create Ec2 Instance
resource "aws_instance" "example_instance" {
  instance_type = var.instance_type
  ami = data.aws_ami.amazon_linux.id
  user_data = filebase64("userdata.txt")
}

## Gather latest ami from AWS 

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*"]
  }
}
