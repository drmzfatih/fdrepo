resource "aws_cloudwatch_event_rule" "console" {
  name        = "s3-put-delete-object"
  description = "Capture put-delete objects in s3 bucket"

  event_pattern = <<EOF
{
  "source": ["aws.s3"],
  "detail-type": ["AWS API Call via CloudTrail"],
  "detail": {
    "eventSource": ["s3.amazonaws.com"],
    "eventName": ["PutObject"]
  }
}
EOF
}