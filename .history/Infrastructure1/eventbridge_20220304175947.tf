resource "aws_cloudwatch_event_rule" "console" {
  name        = "s3-put-delete-object"
  description = "Capture put-delete objects in s3 bucket"

  event_pattern = <<EOF
{
  "source": ["aws.s3"],
  "detail-type": ["AWS API Call via CloudTrail"],
  "detail": {
    "eventSource": ["s3.amazonaws.com"],
    "eventName": ["PutObject"]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "lambda" {
  rule      = aws_cloudwatch_event_rule.console.name
  target_id = "Lambda"
  arn       = aws_lambda_function.s3_copy_function.arn
}
resource "aws_lambda_permission" "allow_cloudwatch_to_call_rw_fallout_retry_step_deletion_lambda" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = aws_lambda_function.function_name.name
  principal = "events.amazonaws.com"
  source_arn = aws_cloudwatch_event_rule.console.arn
}