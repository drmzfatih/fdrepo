resource "aws_s3_bucket" "source_bucket" {
   bucket = "${var.env_name}-src-bucketfatih"
   force_destroy = true
}

resource "aws_s3_bucket" "destination_bucket" {
   bucket = "${var.env_name}-dst-bucketfatih"
   force_destroy = true
}

 resource "aws_s3_bucket_notification" "bucket_terraform_notification" {
bucket = "${aws_s3_bucket.source_bucket.id}"
lambda_function {
lambda_function_arn = "${aws_lambda_function.s3_copy_function.arn}"
events = ["s3:ObjectCreated:*"]
}

depends_on = [ aws_lambda_permission.allow_terraform_bucket ]
}