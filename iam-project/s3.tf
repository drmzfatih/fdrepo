resource "aws_s3_bucket" "msst-1" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g1"
force_destroy = true

  tags = {
    Environment = "Proof of Concept"
    Customer    = "MSST"
  }
}

resource "aws_s3_bucket" "msst-2" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g2"
force_destroy = true

  tags = {
    Environment = "Proof of Concept 2"
    Customer    = "MSST"
  }
}
resource "aws_s3_bucket" "ksg-1" {
  bucket = "kagr-sandbox-ksg-poc-w2-s3-g1"
force_destroy = true

  tags = {
    Environment = "Proof of Concept"
    Customer    = "KSG"
  }
}

resource "aws_s3_bucket" "ksg-2" {
  bucket = "${var.s3_prefix}-fenerbahce"
force_destroy = true
bucket_prefix = "kagr(123123213)"

  tags = {
    Environment = "Proof of Concept 2"
    Customer    = "KSG"
  }
}