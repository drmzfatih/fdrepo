resource "aws_iam_role_policy" "testS3" {
name = "testS3"

policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
   {
    "Effect": "Allow",
    "Action": [
        "s3:ListBucket"
    ],
    "Resource": "arn:aws:s3:::dev-${var.principaltag}*"
   }
  ]
}
EOF
}