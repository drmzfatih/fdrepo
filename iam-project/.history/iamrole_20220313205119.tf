# resource "aws_iam_role" "test_role" {
#   name = "test_role"

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17",
#           "Principal": { "AWS": "arn:aws:iam::123456789012:root" },
#           "Action": "sts:AssumeRole"
          
#       }
#   )

#   tags = {
#     Environment = "Proof of Concept"
#     Customer = "MSST"
#   }
# }

resource "aws_iam_role" "test_role" {
  name = "test_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal": {
                "AWS": "arn:aws:iam::744795182191:root"
            },
            "Condition": {}
        }
    ]
  })

  tags = {
    Environment = "Proof of Concept"
    Customer = "MSST"
  }
}



resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.test_role.name
  policy_arn = aws_iam_policy.policy.arn
}