resource "aws_iam_role" "test_role" {
  name = "test_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "TutorialAssumeRole",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::123456789012:role/tedteam",
            "Condition": {
                "StringEquals": {
                    "iam:ResourceTag/tedteam": "$${aws:PrincipalTag/tedteam}",
                
                }
            }
        }
    ]
},
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}