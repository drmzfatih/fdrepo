resource "aws_iam_policy" "policy" {
  name        = "test_policy1"
  path        = "/"
  description = "My test policy"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
            "Sid": "AllActionsS3SameProjectSameTeam",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "aws:ResourceTag/Environment": "$${aws:PrincipalTag/Environment}",
                    "aws:ResourceTag/Customer": "$${aws:PrincipalTag/Customer}"
                },
                "ForAllValues:StringEquals": {
                    "aws:TagKeys": [
                        "Environment",
                        "Customer"
                    ]
                },
                "StringEqualsIfExists": {
                    "aws:ResourceTag/Environment": "$${aws:PrincipalTag/Environment}",
                    "aws:ResourceTag/Customer": "$${aws:PrincipalTag/Customer}"
                }
            }
        }
    ]
  })
}