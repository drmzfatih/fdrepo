resource "aws_iam_user" "access" {
  name = "hasaccess"
  path = "/system/"

  tags = {
    tag-key = "tedteam"
  }
}

resource "aws_iam_user" "noaccess" {
  name = "noaccess"
  path = "/system/"

  tags = {
    tag-key = "no-access-team"
  }
}

resource "aws_iam_user_policy_attachment" "test-attach" {
  user       = aws_iam_user.access.name
  policy_arn = aws_iam_policy.policy.arn
}