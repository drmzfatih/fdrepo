resource "aws_iam_user" "lb" {
  name = "hasaccess"
  path = "/system/"

  tags = {
    tag-key = "tedteam"
  }
}

resource "aws_iam_user" "lb" {
  name = "noaccess"
  path = "/system/"

  tags = {
    tag-key = "tedteam"
  }
}
