resource "aws_iam_user" "MSST-1" {
  name = "MSST-1"
  path = "/system/"

  tags = {
    Environment = "Proof of Concept"
    Customer = "MSST"
  }
}

resource "aws_iam_user" "KSG-1" {
  name = "Ksg-1"
  path = "/system/"

  tags = {
    Environment = "Proof of Concept"
    Customer = "KSG"
  }
}

