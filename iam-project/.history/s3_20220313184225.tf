resource "aws_s3_bucket" "msst-1" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g1"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "MSST"
  }
}

resource "aws_s3_bucket" "msst-2" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g2"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "MSST"
  }
}
resource "aws_s3_bucket" "ksg-1" {
  bucket = "kagr-sandbox-ksg-poc-w2-s3-g1"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "KSG"
  }
}

resource "aws_s3_bucket" "2-ksg" {
  bucket = "kagr-sandbox-ksg-poc-w2-s3-g2"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "KSG"
  }
}