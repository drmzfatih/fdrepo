resource "aws_s3_bucket" "1-msst" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g1"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "MSST"
  }
}

resource "aws_s3_bucket" "2-msst" {
  bucket = "kagr-sandbox-msst-poc-w2-s3-g2"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "MSST"
  }
}
resource "aws_s3_bucket" "1-ksg" {
  bucket = "kagr-sandbox-ksg-poc-w2-s3-g1"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "KSG"
  }
}

resource "aws_s3_bucket" "2-ksg" {
  bucket = "kagr-sandbox-ksg-poc-w2-s3-g2"


  tags = {
    Environment = "Proof of Concept"
    Customer    = "KSG"
  }
}