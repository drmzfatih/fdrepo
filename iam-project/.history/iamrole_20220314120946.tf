resource "aws_iam_role" "test_role" {
  name = "test_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal": {
                "AWS": "arn:aws:iam::744795182191:root"
            },
            "Condition": {}
        }
    ]
  })

  tags = {
    Environment = "Proof of Concept"
    Customer = "MSST"
  }
}



resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.test_role.name
  policy_arn = aws_iam_policy.policy.arn
}




resource "aws_iam_role" "test_role_2" {
  name = "test_role_2"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal": {
                "AWS": "arn:aws:iam::744795182191:root"
            },
            "Condition": {}
        }
    ]
  })

  tags = {
    Environment = "Proof of Concept 2" 
    Customer = "MSST"
  }
}



resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.test_role_2.name
  policy_arn = aws_iam_policy.policy.arn
}



resource "aws_iam_role" "test_role_3" {
  name = "test_role_3"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal": {
                "AWS": "arn:aws:iam::744795182191:root"
            },
            "Condition": {}
        }
    ]
  })

  tags = {
    Environment = "Proof of Concept" 
    Customer = "KSG"
  }
}



resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.test_role_3.name
  policy_arn = aws_iam_policy.policy.arn
}


esource "aws_iam_role" "test_role_4" {
  name = "test_role_4"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Principal": {
                "AWS": "arn:aws:iam::744795182191:root"
            },
            "Condition": {}
        }
    ]
  })

  tags = {
    Environment = "Proof of Concept 2" 
    Customer = "KSG"
  }
}



resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.test_role_3.name
  policy_arn = aws_iam_policy.policy.arn
}