resource "aws_iam_policy" "policy" {
  name        = "test_policy"
  path        = "/"
  description = "My test policy"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Sid": "TutorialAssumeRole",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::*:role/*",
            "Condition": {
                "StringEquals": {
                    "iam:ResourceTag/access-project": "$${aws:PrincipalTag/access-project}",
                    "iam:ResourceTag/access-team": "$${aws:PrincipalTag/access-team}",
                    "iam:ResourceTag/cost-center": "$${aws:PrincipalTag/cost-center}"
                }
            }
        }
    ]
  })
}