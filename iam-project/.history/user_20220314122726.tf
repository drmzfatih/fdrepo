resource "aws_iam_user" "MSST-1" {
  name = "kagr-sandbox-ksg-poc-w2-s3-g1"
  path = "/system/"

  tags = {
    Environment = "Proof of Concept"
    Customer = "MSST"
  }
}

resource "aws_iam_user" "KSG-1" {
  name = "Ksg-1"
  path = "/system/"
  password = "HELLOWORLD2009@"

  tags = {
    Environment = "Proof of Concept"
    Customer = "KSG"
  }
}



resource "aws_iam_policy_attachment" "test-attach" {
  name       = "test-attachment"
  users      = [aws_iam_user.MSST-1.name, aws_iam_user.KSG-1.name]
  policy_arn = aws_iam_policy.assume_role_policy.arn
}