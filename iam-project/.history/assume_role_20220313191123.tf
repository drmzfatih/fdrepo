resource "aws_iam_policy" "assume_role_policy" {
  name        = "test_policy"
  path        = "/"
  description = "My test policy"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
     {
            "Sid": "TutorialAssumeRole",
            "Effect": "Allow",
            "Action": "sts:AssumeRole",
            "Resource": "arn:aws:iam::*:role/*",
            "Condition": {
                "StringEquals": {
                   "aws:ResourceTag/Environment": "$${aws:PrincipalTag/Environment}",
                    "aws:ResourceTag/Customer": "$${aws:PrincipalTag/Customer}"
                }
            }
        }
    ]
  })
}