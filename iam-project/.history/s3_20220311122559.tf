resource "aws_s3_bucket" "this" {
  count = var.create_bucket ? 1 : 0

   bucket        = "${var.customer_name}-bucket"
  bucket_prefix = var.env_name
}

resource "aws_s3_bucket" "this2" {
  count = var.create_bucket ? 1 : 0

  bucket        = "${var.customer_name}-bucket2"
  bucket_prefix = var.env_name
}

resource "aws_s3_bucket" "this3" {
  count = var.create_bucket ? 1 : 0

   bucket        = "${var.customer_name}-bucket3"
  bucket_prefix = var.env_name
}

resource "aws_s3_bucket" "this4" {
  count = var.create_bucket ? 1 : 0

   bucket        = "${var.customer_name}-bucket4"
  bucket_prefix = var.env_name
}

