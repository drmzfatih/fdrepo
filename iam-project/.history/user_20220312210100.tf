resource "aws_iam_user" "access" {
  name = "hasaccess"
  path = "/system/"

  tags = {
    tag-key = "tedteam"
  }
}

resource "aws_iam_user" "noaccess" {
  name = "noaccess"
  path = "/system/"

  tags = {
    tag-key = "no-access-team"
  }
}
